﻿using System;
using System.Diagnostics;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
            var watch = Stopwatch.StartNew();
            this.ThreeTimesBinomial.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(this.ThreeTimesBinomialFilter());
            watch.Stop();
            this.TimeThreeTimes.Text = watch.Elapsed.ToString();
            watch = Stopwatch.StartNew();
            this.OneTimeBinomial.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(this.OneTimeBinomialFilter());
            watch.Stop();
            this.TimeOneTime.Text = watch.Elapsed.ToString();
            watch = Stopwatch.StartNew();
            this.SeperableFilter.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(this.SeperableBinomialFilter());
            watch.Stop();
            this.TimeSeperable.Text = watch.Elapsed.ToString();
        }

        public Image<Gray, byte> ThreeTimesBinomialFilter()
        {
            var mask = new double[,] { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
            var result = new Image<Gray, byte>(@"C:\Repos\GRA-Exercises\Exercise3\bin\Debug\London.bmp");
            for (int i = 0; i < 3; i++)
            {
                result = this.BinomialFilter(result, mask, 16);
            }
            return result;
        }

        public Image<Gray, byte> OneTimeBinomialFilter()
        {
            var image = new Image<Gray, byte>(@"C:\Repos\GRA-Exercises\Exercise3\bin\Debug\London.bmp");
            var mask = new double[,] { { 1, 6, 15, 20, 15, 6, 1 } };
            var matrix = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseOfArray(mask);
            var mask2 = new double[,] { { 1 }, { 6 }, { 15 }, { 20 }, { 15 }, { 6 }, { 1 } };
            var secondMatrix = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseOfArray(mask2);
            var binomialMask = secondMatrix * matrix;
            return this.BinomialFilter(image, binomialMask.ToArray(), (int)binomialMask.ColumnSums().Sum());
        }

        public Image<Gray, byte> SeperableBinomialFilter()
        {
            var image = new Image<Gray, byte>(@"C:\Repos\GRA-Exercises\Exercise3\bin\Debug\London.bmp");
            var mask = new double[,] { { 1, 6, 15, 20, 15, 6, 1 } };
            var matrix = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseOfArray(mask);
            var mask2 = new double[,] { { 1 }, { 6 }, { 15 }, { 20 }, { 15 }, { 6 }, { 1 } };
            var secondMatrix = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseOfArray(mask2);
            var binomialMask = secondMatrix * matrix;
            var temp = this.BinomialFilter(image, secondMatrix.ToArray(), (int)secondMatrix.ColumnSums().Sum());
            return this.BinomialFilter(image, matrix.ToArray(), (int)matrix.ColumnSums().Sum());
        }

        public Image<Gray, byte> BinomialFilter(Image<Gray, byte> image, double[,] mask, int matrixSum)
        {
            var result = image.Clone();
            var maskRows = mask.GetLength(0);
            var maskColumns = mask.GetLength(1);
            var maskStartRow = (int)Math.Floor(maskRows / 2d) * -1;
            var maskEndRow = maskStartRow * -1;
            var maskStartColumn = (int)Math.Floor(maskColumns / 2d) * -1;
            var maskEndColumn = maskStartColumn * -1;
            for (int i = maskEndRow; i < result.Rows - maskEndRow; i++)
            {
                for (int j = maskEndColumn; j < result.Cols - maskEndColumn; j++)
                {
                    int maskResult = 0;

                    for (int maskRow = 0; maskRow < maskRows; maskRow++)
                    {
                        for (int maskColumn = 0; maskColumn < maskColumns; maskColumn++)
                        {
                            maskResult += ((int)image[i + maskStartRow + maskRow, j + maskStartColumn + maskColumn].Intensity) * (int)mask[maskRow, maskColumn];
                        }
                    }
                    result[i, j] = new Gray(maskResult / matrixSum);
                }
            }
            return result;
        }
    }
}