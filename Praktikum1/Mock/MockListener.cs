﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;
using Praktikum1.Models;

namespace Praktikum1.Utility.Mock
{
    public class MockListener : IListener
    {
        private DispatcherTimer timer;

        public event Action<ImageWithDepth> ImageUpdated;

        public ImageWithDepth Image { get; }

        public ILensParameters LensParameters { get; set; }

        public MockListener()
        {
            this.Image = new ImageWithDepth();
        }

        public void Start()
        {
            //this.imageFromFile = CvInvoke.Imread(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().FullName), @"Mock\images\MockImage.png")).ToImage<Gray, byte>();
            var images = this.GetMockKeyboardImage().ToArray();
            //var images = this.GetGifImages().ToArray();
            var counter = 0;
            var maxCounter = images.Count();
            var imageLock = new object();
            this.timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(10) };
            this.timer.Tick += (s, e) =>
            {
                lock (imageLock)
                {
                    this.Image.Depth = new Models.Image(images[counter].Clone());
                    this.Image.Gray = new Models.Image(images[counter].Clone());
                    counter++;
                    if (counter > maxCounter - 1)
                    {
                        counter = 0;
                    }

                    this.ImageUpdated?.Invoke(this.Image);
                }
            };
            this.timer.Start();
        }

        public IEnumerable<Image<Gray, byte>> GetGifImages()
        {
            for (int i = 0; i < 10; i++)
            {
                yield return CvInvoke.Imread(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().FullName),
                    $"Mock\\images\\frame_0{i}_delay-0.04s.png")).ToImage<Gray, byte>();
            }
        }

        public IEnumerable<Image<Gray, byte>> GetMockKeyboardImage()
        {
            yield return CvInvoke.Imread(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().FullName), "Mock\\images\\MockKeyboard.png")).ToImage<Gray, byte>();
        }
    }
}