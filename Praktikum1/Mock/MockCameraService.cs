﻿using System;
using Praktikum1.Interfaces;
using Praktikum1.Models;

namespace Praktikum1.Utility.Mock
{
    public class MockCameraService : ICameraService
    {
        public ImageWithDepth GetCurrentImage => this.listener.Image;

        public event Action<ImageWithDepth> ImageUpdated;

        private readonly IListener listener;

        public ILensParameters LensParameters => null;

        public MockCameraService(IListener listener)
        {
            this.listener = listener;
            this.listener.ImageUpdated += image =>
            {
                this.ImageUpdated?.Invoke(image);
            };
        }

        public InitializationResult Initialize()
        {
            (this.listener as MockListener).Start();
            return InitializationResult.Success;
        }
    }
}