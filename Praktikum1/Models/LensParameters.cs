﻿using Praktikum1.Interfaces;

namespace Praktikum1.Models
{
    public class LensParameters : ILensParameters
    {
        public float FocalLengthFx { get; set; }

        public float FocalLengthFy { get; set; }

        public float PrincipalPointCx { get; set; }

        public float PrincipalPointCy { get; set; }

        public float DistortionRadialK1 { get; set; }

        public float DistortionRadialK2 { get; set; }

        public float DistortionRadialK3 { get; set; }

        public float DistortionTangentialP1 { get; set; }

        public float DistortionTangentialP2 { get; set; }
    }
}