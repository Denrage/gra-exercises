﻿namespace Praktikum1.Models
{
    public class ImageWithDepth
    {
        public Image Depth { get; set; }

        public Image Gray { get; set; }
    }
}