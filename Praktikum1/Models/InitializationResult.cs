﻿namespace Praktikum1.Models
{
    public enum InitializationResult
    {
        NoCameraFound,
        CouldNotCreateCamera,
        CouldNotInitializeCamera,
        CouldNotReadLensParameters,
        CouldNotRegisterListener,
        Success
    }
}