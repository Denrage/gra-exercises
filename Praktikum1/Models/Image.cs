﻿using System.Collections.Generic;
using Emgu.CV;
using Praktikum1.Interfaces;

namespace Praktikum1.Models
{
    public class Image
    {
        public IImage EmguImage { get; set; }

        public IImage Original { get; private set; }

        public Image(IImage image)
        {
            this.ProcessedImages = new Dictionary<IImageProcessingService, IImage>();
            this.EmguImage = image;
            this.Original = image.Clone() as IImage;
        }

        // TODO: Allow key duplicates?
        public IDictionary<IImageProcessingService, IImage> ProcessedImages { get; }

        public void ProcessImage(IEnumerable<IImageProcessingService> imageProcessingServices)
        {
            foreach (var imageProcessingService in imageProcessingServices)
            {
                this.ProcessedImages[imageProcessingService] = this.EmguImage;
                this.EmguImage = imageProcessingService.ProcessImage(this.EmguImage);
            }
        }
    }
}