﻿using System;
using System.Linq;
using Praktikum1.Interfaces;
using Praktikum1.Models;
using RoyaleDotNet;

namespace Praktikum1.Royale
{
    public class CameraService : ICameraService
    {
        private readonly CameraManager cameraManager;
        private CameraDevice cameraDevice;
        private readonly IListener listener;

        public event Action<ImageWithDepth> ImageUpdated;

        public ImageWithDepth GetCurrentImage => this.listener.Image;

        public ILensParameters LensParameters { get; private set; }

        public CameraService(IListener listener)
        {
            this.cameraManager = new CameraManager();
            this.listener = listener;
            this.listener.ImageUpdated += this.ImageUpdated;
        }

        private void SetCameraSettings(CameraDevice cameraDevice) => this.cameraDevice.SetExposureMode(ExposureMode.Automatic);

        public InitializationResult Initialize()
        {
            var camlist = this.cameraManager.GetConnectedCameraList();

            if (!camlist.Any())
            {
                return InitializationResult.NoCameraFound;
            }

            this.cameraDevice = this.cameraManager.CreateCamera(camlist.First());

            if (this.cameraDevice is null)
            {
                return InitializationResult.CouldNotCreateCamera;
            }

            this.SetCameraSettings(this.cameraDevice);
            var status = this.cameraDevice.Initialize();

            if (status != CameraStatus.SUCCESS)
            {
                return InitializationResult.CouldNotInitializeCamera;
            }

            status = this.cameraDevice.GetLensParameters(out var lensParameters);
            if (status != CameraStatus.SUCCESS)
            {
                return InitializationResult.CouldNotReadLensParameters;
            }

            this.LensParameters = lensParameters.ConvertParameters();

            if (!(this.listener is IDepthDataListener royalListener) || this.cameraDevice.RegisterDepthDataListener(royalListener) != CameraStatus.SUCCESS)
            {
                return InitializationResult.CouldNotRegisterListener;
            }

            return InitializationResult.Success;
        }

        public void StartCapture()
        {
            if (this.cameraDevice.StartCapture() != CameraStatus.SUCCESS)
            {
                throw new InvalidOperationException("Could not start capture");
            }
        }

        public void StopCapture()
        {
            if (this.cameraDevice.StopCapture() != CameraStatus.SUCCESS)
            {
                throw new InvalidOperationException("Could not stop capture");
            }
        }
    }
}