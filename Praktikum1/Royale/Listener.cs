﻿using Emgu.CV.Structure;
using Praktikum1.Utility;
using RoyaleDotNet;

namespace Praktikum1.Royale
{
    public class Listener : Listener<Bgr, Gray>, IDepthDataListener
    {
        public void OnNewData(DepthData data) =>
            this.UpdateImage(
                data.width,
                data.height,
                (y, x) => (data.points[y * x].z, data.points[y * x].grayValue),
                (y, x) => this.ShouldSet(data, y, x));

        private bool ShouldSet(DepthData data, int y, int x) =>
            data.points[y * x].depthConfidence > 0;

        protected override Gray CreateGrayColor(float grayValue) => new Gray(grayValue);

        protected override Bgr CreateZColor(float zValue) =>
            zValue > 0.25 ? new Bgr(0, 0, 255) : new Bgr(0, 0, 0);
    }
}