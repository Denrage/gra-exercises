﻿namespace Praktikum1.Royale
{
    public static class Extensions
    {
        public static Models.LensParameters ConvertParameters(this RoyaleDotNet.LensParameters lensParameters)
            => new Models.LensParameters()
            {
                DistortionRadialK1 = lensParameters.DistortionRadial.K1,
                DistortionRadialK2 = lensParameters.DistortionRadial.K2,
                DistortionRadialK3 = lensParameters.DistortionRadial.K3,
                DistortionTangentialP1 = lensParameters.DistortionTangential.P1,
                DistortionTangentialP2 = lensParameters.DistortionTangential.P2,
                FocalLengthFx = lensParameters.FocalLength.FX,
                FocalLengthFy = lensParameters.FocalLength.FY,
                PrincipalPointCx = lensParameters.PrincipalPoint.CX,
                PrincipalPointCy = lensParameters.PrincipalPoint.CY,
            };
    }
}