﻿using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Praktikum1.Utility
{
    public static class ImageContourAnalyzer
    {
        public static IEnumerable<Rectangle> GetRectangles(IImage image)
        {
            var contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(image, contours, null, RetrType.List, ChainApproxMethod.ChainApproxNone);
            var contourArray = contours.ToArrayOfArray();
            var result = new List<Rectangle>();
            for (int i = 0; i < contours.Size / 2; i++)
            {
                result.Add(new Rectangle(contourArray[i]));
            }

            return result;
        }

        public static IImage ProcessImage(IImage image)
        {
            var output = new Image<Gray, byte>(image.Size);
            var contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(image, contours, null, RetrType.List, ChainApproxMethod.ChainApproxNone);
            output.Draw(contours, -1, new Gray(byte.MaxValue));
            return output;
        }
    }

    public class Rectangle
    {
        public IEnumerable<System.Drawing.Point> Points;

        public int Width { get; set; }

        public int Height { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public Rectangle(IEnumerable<System.Drawing.Point> points)
        {
            this.Points = points;
            var minX = this.Points.OrderBy(x => x.X).First();
            var minY = this.Points.OrderBy(x => x.Y).First();
            var min = minX.X < minY.Y ? minX : minY;
            var maxX = this.Points.OrderByDescending(x => x.X).First();
            var maxY = this.Points.OrderByDescending(x => x.Y).First();
            var max = maxX.X > maxY.Y ? maxX : maxY;
            this.Width = max.X - min.X;
            this.Height = maxY.Y - min.Y;
            this.X = min.X;
            this.Y = min.Y;
        }
    }
}