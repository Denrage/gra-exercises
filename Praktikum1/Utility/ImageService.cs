﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Emgu.CV;
using Praktikum1.Interfaces;
using Praktikum1.ProcessingServices;

namespace Praktikum1.Utility
{
    public class ImageService
    {
        private readonly ICameraService cameraService;

        public ImageService(ICameraService cameraService)
        {
            this.cameraService = cameraService;
        }

        public IEnumerable<Rectangle> Rectangles { get; private set; }

        public async Task<IEnumerable<Rectangle>> CaptureMainArea()
        {
            Models.Image image = null;
            void setImage(Models.ImageWithDepth im)
            {
                image = im.Gray;
            }

            this.cameraService.ImageUpdated += setImage;

            var task = Task.Run(() =>
            {
                while (image is null)
                {
                    Thread.Sleep(10);
                }
            });

            await task;
            this.cameraService.ImageUpdated -= setImage;

            image.EmguImage = this.ProcessImage(image.EmguImage, new IImageProcessingService[]
            {
                new ImageDistorter(this.cameraService.LensParameters),
                new ImageContrastAdjusterExcludeZeroValues(),
                new ImageEdgeFilter(),
            });

            var rectangles = ImageContourAnalyzer.GetRectangles(image.EmguImage);
            this.Rectangles = rectangles;
            return rectangles;
        }

        public IImage ProcessImage(IImage image, IEnumerable<IImageProcessingService> imageProcessingServices)
        {
            var temp = image.Clone() as IImage;
            foreach (var imageProcessingService in imageProcessingServices)
            {
                temp = imageProcessingService.ProcessImage(temp);
            }

            return temp;
        }

        //public void Initialize() => this.GrayImage.ProcessImage(new IImageProcessingService[]
        //    {
        //        new ImageDistorter(this.cameraService.LensParameters),
        //        new ImageContrastAdjuster(),
        //        new ImageColormapApplicator(),
        //    });
    }
}