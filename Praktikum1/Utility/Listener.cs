﻿using System;
using System.Runtime.CompilerServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Praktikum1.Interfaces;
using Praktikum1.Models;

namespace Praktikum1.Utility
{
    public abstract class Listener<TColorZ, TColorGray> : IListener
        where TColorZ : struct, IColor
        where TColorGray : struct, IColor
    {
        public Models.ImageWithDepth Image { get; } = new Models.ImageWithDepth();

        public ILensParameters LensParameters { get; set; }

        public event Action<Models.ImageWithDepth> ImageUpdated;

        protected abstract TColorZ CreateZColor(float zValue);

        protected abstract TColorGray CreateGrayColor(float grayValue);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void UpdateImage(int width, int height, Func<int, int, (float z, ushort gray)> getPointValues, Func<int, int, bool> shouldSet)
        {
            var zImage = new Mat(
                new System.Drawing.Size(width, height),
                DepthType.Cv32F, 1)
                .ToImage<TColorZ, byte>();
            var grayImage = new Mat(
                new System.Drawing.Size(width, height),
                DepthType.Cv8U, 1)
                .ToImage<TColorGray, byte>();
            for (int y = 0; y < zImage.Rows; y++)
            {
                for (int x = 0; x < zImage.Cols; x++)
                {
                    var (z, gray) = getPointValues(y, x);
                    if (shouldSet(y, x))
                    {
                        zImage[y, x] = this.CreateZColor(z);
                        grayImage[y, x] = this.CreateGrayColor(gray);
                    }
                }
            }

            this.Image.Depth = new Image(zImage);
            this.Image.Gray = new Image(grayImage);

            this.ImageUpdated?.Invoke(this.Image);
        }
    }
}