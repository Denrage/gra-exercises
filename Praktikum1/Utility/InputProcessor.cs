﻿using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace Praktikum1.Utility
{
    public static class InputProcessor
    {
        public static void ContrastAdjustment(Image<Bgr, byte> depthImage)
        {
            var min = double.MaxValue;
            var max = double.MinValue;
            var nullPoint = new Point();

            var zeroImage = depthImage.Clone();
            var nullColor = new Bgr(0, 0, 0);
            for (int columnIndex = 0; columnIndex < depthImage.Cols; columnIndex++)
            {
                for (int rowIndex = 0; rowIndex < depthImage.Rows; rowIndex++)
                {
                    zeroImage[rowIndex, columnIndex] = nullColor;
                }
            }

            CvInvoke.Compare(depthImage, zeroImage, depthImage, CmpType.NotEqual);

            // TODO: Exclude 0 values from min
            CvInvoke.MinMaxLoc(depthImage, ref min, ref max, ref nullPoint, ref nullPoint);
            CvInvoke.ConvertScaleAbs(depthImage, depthImage, 255 / (max - min), -min * 255 / (max - min));
        }

        public static void ApplyColorMap(Image<Bgr, byte> depthImage)
        {
            Mat output = Mat.Zeros(depthImage.Rows, depthImage.Cols, DepthType.Cv8U, 3);
            CvInvoke.ApplyColorMap(depthImage, output, ColorMapType.Cool);
        }
    }
}