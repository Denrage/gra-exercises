﻿using System.Collections.Generic;
using Emgu.CV;

namespace Praktikum1.Utility
{
    public class CaptureWriter
    {
        private readonly string fileName;
        private readonly int fps;

        public CaptureWriter(string fileName, int fps)
        {
            this.fileName = fileName;
            this.fps = fps;
        }

        public void Write(List<Models.ImageWithDepth> images)
        {
            using (var zWriter = new VideoWriter("z_" + this.fileName, this.fps, images[0].Depth.EmguImage.Size, true))
            using (var grayWriter = new VideoWriter("gray_" + this.fileName, this.fps, images[0].Gray.EmguImage.Size, false))
            {
                foreach (var item in images)
                {
                    if (item.Depth.EmguImage is CvArray<byte> zImage)
                    {
                        zWriter.Write(zImage.Mat);
                    }

                    if (item.Gray.EmguImage is CvArray<byte> grayImage)
                    {
                        grayWriter.Write(grayImage.Mat);
                    }
                }
            }
        }
    }
}