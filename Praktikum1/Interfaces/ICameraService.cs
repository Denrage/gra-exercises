﻿using System;
using Praktikum1.Models;

namespace Praktikum1.Interfaces
{
    public interface ICameraService
    {
        ILensParameters LensParameters { get; }

        Models.ImageWithDepth GetCurrentImage { get; }

        event Action<Models.ImageWithDepth> ImageUpdated;

        InitializationResult Initialize();
    }
}