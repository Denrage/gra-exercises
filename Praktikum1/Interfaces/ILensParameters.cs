﻿namespace Praktikum1.Interfaces
{
    public interface ILensParameters
    {
        float FocalLengthFx { get; }

        float FocalLengthFy { get; }

        float PrincipalPointCx { get; }

        float PrincipalPointCy { get; }

        float DistortionRadialK1 { get; }

        float DistortionRadialK2 { get; }

        float DistortionRadialK3 { get; }

        float DistortionTangentialP1 { get; }

        float DistortionTangentialP2 { get; }
    }
}