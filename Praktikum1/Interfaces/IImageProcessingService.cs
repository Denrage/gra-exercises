﻿using Emgu.CV;

namespace Praktikum1.Interfaces
{
    public interface IImageProcessingService
    {
        IImage ProcessImage(IImage image);
    }
}