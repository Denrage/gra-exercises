﻿using System;

namespace Praktikum1.Interfaces
{
    public interface IListener
    {
        event Action<Models.ImageWithDepth> ImageUpdated;

        Models.ImageWithDepth Image { get; }
    }
}