﻿using System.Collections.Generic;
using System.Windows;
using Praktikum1.Interfaces;
using Praktikum1.Utility;

namespace Praktikum1.Views
{
    /// <summary>
    /// Interaction logic for Capture.xaml
    /// </summary>
    public partial class Capture : Window
    {
        private CaptureWriter captureWriter;
        private readonly List<Models.ImageWithDepth> images = new List<Models.ImageWithDepth>();
        private readonly ICameraService cameraService;

        public Capture(ICameraService cameraService)
        {
            this.InitializeComponent();
            this.Stop.IsEnabled = false;
            this.captureWriter = new CaptureWriter("test.avi", 3);
            this.cameraService = cameraService;
        }

        private void AddImage(Models.ImageWithDepth image) => this.images.Add(image);

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            this.Start.IsEnabled = false;
            this.Stop.IsEnabled = true;
            this.cameraService.ImageUpdated += this.AddImage;
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            this.Stop.IsEnabled = false;
            this.Start.IsEnabled = true;
            this.cameraService.ImageUpdated -= this.AddImage;
        }

        private void Show_Click(object sender, RoutedEventArgs e) => new Video(this.images).Show();

        private void Save_Click(object sender, RoutedEventArgs e) => this.captureWriter.Write(this.images);
    }
}