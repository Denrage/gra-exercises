﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Praktikum1.Interfaces;
using Praktikum1.LightInject;
using Praktikum1.Royale;
using Praktikum1.Utility.Mock;

namespace Praktikum1.Views
{
    /// <summary>
    /// Interaction logic for Bootstrap.xaml
    /// </summary>
    public partial class Bootstrap : Window
    {
        private readonly IEnumerable<(Type Interface, Type Implementation)> GlobalDependencies = Enumerable.Empty<(Type Interface, Type Implementation)>();
        private readonly ServiceContainer container;
        public Bootstrap()
        {
            InitializeComponent();
            this.container = new ServiceContainer();
        }

        private void Mock_Click(object sender, RoutedEventArgs e)
        {
            var mockDependencies = new[]
            {
                (typeof(ICameraService), typeof(MockCameraService)),
                (typeof(IListener), typeof(MockListener))
            };
            this.Start(mockDependencies);
        }

        private void Start(params (Type Interface, Type Implementation)[] types)
        {
            this.RegisterDependencies(this.GlobalDependencies.Concat(types).ToArray());
            this.OpenMainWindow();
        }

        private void OpenMainWindow()
        {
            this.Visibility = Visibility.Hidden;
            new MainWindow(this.container.GetInstance<ICameraService>()).ShowDialog();
            Environment.Exit(0);
        }

        private void Royale_Click(object sender, RoutedEventArgs e)
        {
            var royaleDependencies = new[]
            {
                (typeof(ICameraService), typeof(CameraService)),
                (typeof(IListener), typeof(Listener))
            };
            this.Start(royaleDependencies);
        }

        private void RegisterDependencies(params (Type Interface, Type Implementation)[] types)
        {
            foreach (var type in types)
            {
                this.container.Register(type.Interface, type.Implementation, new PerContainerLifetime());
            }
        }
    }
}
