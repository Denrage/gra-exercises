﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using ImageProcessingLibrary;

namespace Praktikum1.Views
{
    /// <summary>
    /// Interaction logic for Video.xaml
    /// </summary>
    public partial class Video : Window
    {
        private readonly CancellationTokenSource videoTokenSource = new CancellationTokenSource();

        public Video(IEnumerable<Models.ImageWithDepth> images)
        {
            this.InitializeComponent();
            Task.Run(() => this.ShowVideo(images, this.videoTokenSource.Token));
        }

        private void ShowVideo(IEnumerable<Models.ImageWithDepth> images, CancellationToken token)
        {
            int current = 0;
            var bitmaps = new List<(BitmapSource Z, BitmapSource Gray)>();
            this.Dispatcher.Invoke(() =>
            {
                Parallel.ForEach(images, image =>
                {
                    bitmaps.Add((
                        ImageHelper.ToBitmapSource(image.Depth.EmguImage),
                        ImageHelper.ToBitmapSource(image.Gray.EmguImage)
                        ));
                });
            });

            while (!token.IsCancellationRequested)
            {
                Thread.Sleep(100);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.ZImage.Source = bitmaps[current].Z;
                    this.GrayImage.Source = bitmaps[current].Gray;

                    if (current == bitmaps.Count - 1)
                    {
                        current = 0;
                    }
                    else
                    {
                        current++;
                    }
                });
            }
        }

        protected override void OnClosing(CancelEventArgs e) => this.videoTokenSource.Cancel();
    }
}