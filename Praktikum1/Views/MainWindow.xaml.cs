﻿using System.Windows;
using Praktikum1.Interfaces;
using Praktikum1.Models;
using Praktikum1.Utility;
using Praktikum1.Views;

namespace Praktikum1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ICameraService cameraService;

        public MainWindow(ICameraService cameraService)
        {
            this.cameraService = cameraService;
            var initializationResult = this.cameraService.Initialize();
            if (initializationResult != InitializationResult.Success)
            {
                MessageBox.Show($"Camera initialization failed: {initializationResult.ToString()}");
            }
            this.InitializeComponent();
        }

        private async void LiveCam_Click(object sender, RoutedEventArgs e) => new LiveCam(this.cameraService).Show();

        private void Process_Click(object sender, RoutedEventArgs e) => new Process(new ImageService(this.cameraService)).Show();

        private void Capture_Click(object sender, RoutedEventArgs e) => new Capture(this.cameraService).Show();
    }
}