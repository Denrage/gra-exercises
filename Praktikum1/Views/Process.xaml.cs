﻿using System;
using System.Windows;
using System.Windows.Media;
using Praktikum1.Utility;

namespace Praktikum1.Views
{
    /// <summary>
    /// Interaction logic for Process.xaml
    /// </summary>
    public partial class Process : Window
    {
        private readonly ImageService imageService;

        public Process(ImageService imageService)
        {
            this.InitializeComponent();
            this.Loaded += this.Process_Loaded;
            this.imageService = imageService;
        }

        private async void Process_Loaded(object sender, RoutedEventArgs e)
        {
            var rectangles = await this.imageService.CaptureMainArea();
            var random = new Random();
            foreach (var rectangle in rectangles)
            {
                var rect = new System.Windows.Shapes.Rectangle()
                {
                    Width = rectangle.Width,
                    Height = rectangle.Height,
                    Fill = new SolidColorBrush(Color.FromArgb(255, (byte)random.Next(0, 255), (byte)random.Next(0, 255), (byte)random.Next(0, 255)))
                };
                System.Windows.Controls.Canvas.SetLeft(rect, rectangle.X);
                System.Windows.Controls.Canvas.SetTop(rect, rectangle.Y);
                this.Rectangles.Children.Add(rect);
            }
        }
    }
}