﻿using System.Windows;
using ImageProcessingLibrary;
using Praktikum1.Interfaces;

namespace Praktikum1
{
    /// <summary>
    /// Interaction logic for LiveCam.xaml
    /// </summary>
    public partial class LiveCam : Window
    {
        public LiveCam(ICameraService cameraService)
        {
            this.InitializeComponent();
            bool needsSwap = false;
            cameraService.ImageUpdated += image =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.Image.Source = !needsSwap ? ImageHelper.ToBitmapSource(image.Depth.EmguImage) : ImageHelper.ToBitmapSource(image.Gray.EmguImage);
                        this.GrayImage.Source = !needsSwap ? ImageHelper.ToBitmapSource(image.Gray.EmguImage) : ImageHelper.ToBitmapSource(image.Depth.EmguImage);
                        //needsSwap = !needsSwap;
                    });
                };
        }
    }
}