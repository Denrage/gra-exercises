﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;

namespace Praktikum1.ProcessingServices
{
    public class ImageColormapApplicator : IImageProcessingService
    {
        public IImage ProcessImage(IImage image)
        {
            var coloredImage = new Image<Bgr, byte>(image.Size);
            CvInvoke.ApplyColorMap(image, coloredImage, ColorMapType.Rainbow);
            return coloredImage;
        }
    }
}