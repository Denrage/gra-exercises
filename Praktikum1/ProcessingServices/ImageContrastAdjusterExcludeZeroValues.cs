﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace Praktikum1.ProcessingServices
{
    internal class ImageContrastAdjusterExcludeZeroValues : ImageContrastAdjuster
    {
        protected override void BeforeContrastChange(IImage image, ref double min, ref double max, ref Point nullPoint)
        {
            var mask = new Image<Gray, byte>(image.Size);

            CvInvoke.Compare(image, new ScalarArray(0), mask, CmpType.NotEqual);

            CvInvoke.MinMaxLoc(image, ref min, ref max, ref nullPoint, ref nullPoint, mask);
        }
    }
}