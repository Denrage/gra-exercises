﻿using Emgu.CV;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;

namespace Praktikum1.ProcessingServices
{
    public class ImageDistorter : IImageProcessingService
    {
        private readonly ILensParameters lensParameters;

        public ImageDistorter(ILensParameters lensParameters)
        {
            this.lensParameters = lensParameters;
        }

        public IImage ProcessImage(IImage image)
        {
            if (this.lensParameters == null)
            {
                return image;
            }

            var result = new Image<Gray, byte>(image.Size);
            var cameraMatrix = new Matrix<double>(3, 3)
            {
                [0, 0] = this.lensParameters.FocalLengthFx,
                [0, 1] = 0,
                [0, 2] = this.lensParameters.PrincipalPointCx,
                [1, 0] = 0,
                [1, 1] = this.lensParameters.FocalLengthFy,
                [1, 2] = this.lensParameters.PrincipalPointCy,
                [2, 0] = 0,
                [2, 1] = 0,
                [2, 2] = 1
            };

            var distortionCoefficients = new Matrix<double>(1, 5)
            {
                [0, 0] = this.lensParameters.DistortionRadialK1,
                [0, 1] = this.lensParameters.DistortionRadialK2,
                [0, 2] = this.lensParameters.DistortionTangentialP1,
                [0, 3] = this.lensParameters.DistortionTangentialP2,
                [0, 4] = this.lensParameters.DistortionRadialK3
            };
            CvInvoke.Undistort(image, result, cameraMatrix, distortionCoefficients);
            return result;

        }
    }
}