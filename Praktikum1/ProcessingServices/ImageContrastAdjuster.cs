﻿using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;

namespace Praktikum1.ProcessingServices
{
    public class ImageContrastAdjuster : IImageProcessingService
    {
        public IImage ProcessImage(IImage image) => this.AdjustContrast(image);

        public IImage AdjustContrast(IImage image)
        {
            var min = double.MaxValue;
            var max = double.MinValue;
            var nullPoint = new Point();
            var result = new Image<Gray, byte>(image.Size);
            this.BeforeContrastChange(image, ref min, ref max, ref nullPoint);
            CvInvoke.ConvertScaleAbs(image, result, 255d / (max - min), -min * 255d / (max - min));
            return result;
        }

        protected virtual void BeforeContrastChange(IImage image, ref double min, ref double max, ref Point nullPoint) => CvInvoke.MinMaxLoc(image, ref min, ref max, ref nullPoint, ref nullPoint);
    }
}