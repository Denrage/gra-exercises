﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;

namespace Praktikum1.ProcessingServices
{
    public class ImageBinaryConverter : IImageProcessingService
    {
        public IImage ProcessImage(IImage image)
        {
            var output = new Image<Gray, byte>(image.Size);
            CvInvoke.Threshold(image, output, 0, byte.MaxValue, ThresholdType.Binary | ThresholdType.Otsu);
            return output;
        }
    }
}