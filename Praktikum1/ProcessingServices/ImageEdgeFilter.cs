using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Praktikum1.Interfaces;

namespace Praktikum1.ProcessingServices
{
    public class ImageEdgeFilter : IImageProcessingService
    {
        private readonly double sigma;

        public ImageEdgeFilter(double sigma = 0.33)
        {
            this.sigma = sigma;
        }

        public IImage ProcessImage(IImage image)
        {
            var median = this.GetMedian((Image<Gray, byte>) image);
            var lower = Math.Max(0, (1.0 - this.sigma) * median);
            var upper = Math.Min(255, (1.0 + this.sigma) * median);
            var temp = new Image<Gray, byte>(image.Size);
            var output = new Image<Gray, byte>(image.Size);
            CvInvoke.GaussianBlur(image, temp, new Size(3, 3), 0);
            // already converts to binary image
            CvInvoke.Canny(temp, output, lower, upper);
            return output;
        }

        private double GetMedian(Image<Gray, byte> image)
        {
            var temp = new byte[image.Rows*image.Cols];
            System.Buffer.BlockCopy(image.Data, 0, temp, 0, image.Rows*image.Cols);
            return this.MedianHelper(temp);
        }

        // TODO: Performance optimization O(n*log(n)) --> O(n)
        private double MedianHelper(byte[] array)
        {
            var ordered = array.OrderBy(element => element).ToArray();
            if (ordered.Length % 2 == 1)
            {
                return ordered[ordered.Length / 2];
            }
            else
            {
                return (double)(ordered[ordered.Length / 2 - 1] + ordered[ordered.Length / 2]) / 2;
            }
        }
    }
}