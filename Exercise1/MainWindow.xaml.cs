﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string imagePath = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            "Images",
            "zelle_grau.bmp");

        public MainWindow()
        {
            this.InitializeComponent();
            this.Image.Source = new BitmapImage(new Uri(this.imagePath));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var image = this.CreateImage();
            CvInvoke.BitwiseNot(image, image);
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var image = this.CreateImage();
            var flattenImage = FlattenImage<Gray>(image).ToList().Select(x => x.Intensity).ToList();
            image.AvgSdv(out var average, out var averageStandardDeviation);
            var builder = new StringBuilder();
            builder.Append("Image Width: ").Append(image.Width).AppendLine();
            builder.Append("Image Height: ").Append(image.Height).AppendLine();
            builder.Append("Channel Count: ").Append(image.NumberOfChannels).AppendLine();
            builder.Append("Average Color/StandardDeviation: ")
                .Append(Math.Round(average.Intensity, 4))
                .Append(" / ")
                .Append(Math.Round(averageStandardDeviation.V0))
                .AppendLine();
            builder.Append("Biggest/Lowest Gray: ")
                .Append(flattenImage.Max())
                .Append(" / ")
                .Append(flattenImage.Min())
                .AppendLine();
            builder.Append("Number of pixels more intense than median: ")
                .Append(flattenImage.Count(x => x > average.Intensity))
                .AppendLine();
            MessageBox.Show(builder.ToString());
        }

        private Image<Gray, byte> CreateImage()
        {
            var matrix = CvInvoke.Imread(this.imagePath, Emgu.CV.CvEnum.ImreadModes.AnyColor);
            var image = matrix.ToImage<Gray, byte>();
            return image;
        }

        private static IEnumerable<T> FlattenImage<T>(Image<T, byte> image)
            where T : struct, IColor
        {
            var result = new T[image.Rows * image.Cols];
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    result[i + j] = image[i, j];
                }
            }

            return result;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var image = this.CreateImage();
            var average = image.GetAverage();
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    var gray = image[i, j];
                    if (gray.Intensity > average.Intensity)
                    {
                        gray.Intensity = 255;
                        image[i, j] = gray;
                    }
                }
            }

            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var image = CvInvoke.Imread(this.imagePath, Emgu.CV.CvEnum.ImreadModes.AnyColor).ToImage<Bgr, byte>();
            var average = image.GetAverage();
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    var bgr = image[i, j];
                    if (bgr.Blue > average.Blue)
                    {
                        image[i, j] = new Bgr(0, 0, 255);
                    }
                }
            }
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
        }
    }
}