﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using ImageProcessingLibrary;

namespace Exercise5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Image<Gray, byte> image;
        public MainWindow()
        {
            InitializeComponent();
            this.image = CvInvoke.Imread(@"C:\Workspaces\GRA\praktika\gra-exercises\Exercise5\bin\Debug\images\schrift.bmp", ImreadModes.Grayscale).ToImage<Gray, byte>();
            this.Threshold(15, byte.MaxValue, ThresholdType.Binary);
            this.Threshold(120, byte.MaxValue, ThresholdType.Binary);
            this.Threshold(150, byte.MaxValue, ThresholdType.Binary);
            this.Threshold(0, byte.MaxValue, ThresholdType.Binary | ThresholdType.Otsu);

            this.ThresholdCorrected(3, byte.MaxValue, ThresholdType.BinaryInv);
            this.ThresholdCorrected(5, byte.MaxValue, ThresholdType.BinaryInv);
            this.ThresholdCorrected(8, byte.MaxValue, ThresholdType.BinaryInv);
            this.ThresholdCorrected(10, byte.MaxValue, ThresholdType.BinaryInv);
            this.ThresholdCorrected(0, byte.MaxValue, ThresholdType.BinaryInv | ThresholdType.Otsu);

        }

        private void Threshold(int threshold, int max,
            ThresholdType type)
        {
            var temp = image.CopyBlank();
            CvInvoke.Threshold(image, temp, threshold, max, type);
            CvInvoke.Imshow($"Threshold {threshold}, {type}", temp);
        }

        private void ThresholdCorrected(int threshold, int max,
            ThresholdType type)
        {
            var temp = this.image.CopyBlank();
            var background = this.image.CopyBlank();
            CvInvoke.MedianBlur(this.image, background, 21);
            CvInvoke.Subtract(background, image, temp );

            CvInvoke.Threshold(temp, temp, threshold, max, type);
            CvInvoke.Imshow($"ThresholdCorrected {threshold}, {type}", temp);
        }
    }
}
