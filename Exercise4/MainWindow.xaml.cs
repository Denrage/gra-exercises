﻿using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;
using ImageProcessingLibrary;

namespace Exercise4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.Execute<Gray, byte>();
        }

        private void Execute<TColor, TDepth>()
            where TColor : struct, IColor
            where TDepth : new()
        {
            var originalImage = new Image<TColor, TDepth>(@"C:\Repos\GRA-Exercises\Exercise4\images\muenzen.bmp");
            var image = originalImage.Clone();
            new ImageView("Original", image).Show();
            image = this.Gaussian(image);
            new ImageView("After Blur", image).Show();
            image = this.Sobel(image);
            new ImageView("After Sobel", image).Show();
            new ImageView("Difference", originalImage.Sub(image)).Show();
            image = this.Threshold(image);
            new ImageView("After Threshold", image).Show();
        }

        private Image<TColor, TDepth> Gaussian<TColor, TDepth>(Image<TColor, TDepth> image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var temp = new Image<TColor, TDepth>(image.Size);
            CvInvoke.GaussianBlur(image, temp, new System.Drawing.Size(5, 5), 1);
            return temp;
        }

        private Image<TColor, TDepth> Sobel<TColor, TDepth>(Image<TColor, TDepth> image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var temp = new Image<TColor, TDepth>(image.Size);
            var temp2 = new Image<TColor, TDepth>(image.Size);
            var result = new Image<TColor, TDepth>(image.Size);
            CvInvoke.Sobel(image, temp, Emgu.CV.CvEnum.DepthType.Cv32F, 1, 0);
            CvInvoke.Sobel(image, temp2, Emgu.CV.CvEnum.DepthType.Cv32F, 0, 1);
            CvInvoke.Pow(temp, 2, temp);
            CvInvoke.Pow(temp2, 2, temp2);
            var tempAdd = new Image<TColor, TDepth>(image.Size);
            CvInvoke.Add(temp, temp2, tempAdd);
            CvInvoke.Pow(tempAdd, 0.5, result);
            return ImageHelper.AdjustContrast<TColor, TDepth>(result);
        }

        private Image<TColor, TDepth> Threshold<TColor, TDepth>(Image<TColor, TDepth> image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var temp = new Image<TColor, TDepth>(image.Size);
            CvInvoke.Threshold(image, temp, 100, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            return temp;
        }
    }
}