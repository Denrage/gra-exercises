﻿using System.Windows;
using Emgu.CV;
using ImageProcessingLibrary;

namespace Exercise4
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : Window
    {
        public ImageView(string title, IImage image)
        {
            this.InitializeComponent();
            this.Title = title;

            this.Image.Source = ImageHelper.ToBitmapSource(image);
        }
    }
}