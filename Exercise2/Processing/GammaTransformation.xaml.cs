﻿using System;
using System.Text;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise2
{
    /// <summary>
    /// Interaction logic for GammaTransformation.xaml
    /// </summary>
    public partial class GammaTransformation : Window
    {
        private Image<Gray, byte> image;

        public GammaTransformation(Image<Gray, byte> image)
        {
            this.image = image;
            this.InitializeComponent();
            this.SetImage(image);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.AutoUpdate.IsChecked == true)
            {
                this.UpdateImage(e.NewValue);
            }
        }

        private void UpdateImage(double newValue, bool updateText = false)
        {
            if (this.IsQuadratic.IsChecked == true)
            {
                this.QuadraticGamma(this.image.Copy(), newValue / 100, updateText);
            }
            else
            {
                this.LinearGamma(this.image.Copy(), newValue / 100, updateText);
            }
        }

        private void LinearGamma(Image<Gray, byte> image, double gamma, bool updateText = false)
        {
            image = image.Mul(gamma);
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            if (updateText)
            {
                this.ImageInformation.Text = this.GetInformation(image);
            }
        }

        private void QuadraticGamma(Image<Gray, byte> image, double gamma, bool updateText = false)
        {
            CvInvokeLookUp(image, gamma);
            //OwnLookup(image, gamma);

            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            if (updateText)
            {
                this.ImageInformation.Text = this.GetInformation(image);
            }
        }

        private static void CvInvokeLookUp(Image<Gray, byte> image, double gamma)
        {
            var temp = new double[256];
            //var lookUp = new Mat(1, 256, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            //for (int i = 0; i < 256; i++)
            //{
            //    temp[i] = 255 * Math.Pow((double)i / 255f, gamma);
            //}
            var lookUp = new Mat(1, 256, Emgu.CV.CvEnum.DepthType.Cv8U, 1).ToImage<Gray, byte>();
            for (int i = 0; i < 256; i++)
            {
                lookUp[0, i] = new Gray(255 * Math.Pow((double)i / 255f, gamma));
            }
            CvInvoke.LUT(image, lookUp, image);
        }

        private static void OwnLookup(Image<Gray, byte> image, double gamma)
        {
            var lookUp = new double[256];
            for (int i = 0; i < 256; i++)
            {
                lookUp[i] = 255 * Math.Pow((double)i / 255f, gamma);
            }
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    var temp = image[i, j];
                    temp.Intensity = lookUp[(int)temp.Intensity];
                    image[i, j] = temp;
                }
            }
        }

        public string GetInformation(Image<Gray, byte> image)
        {
            image.AvgSdv(out var average, out var standardDeviation);
            var builder = new StringBuilder();
            builder.Append("Median Grayscale: ").AppendLine(average.ToString());
            builder.Append("Standard Deviation: ").AppendLine(standardDeviation.V0.ToString());
            return builder.ToString();
        }

        public void SetImage(Image<Gray, byte> image)
        {
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            this.ImageInformation.Text = this.GetInformation(image);
        }

        private void Button_Click(object sender, RoutedEventArgs e) => this.UpdateImage(this.Slider.Value, true);
    }
}