﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Emgu.CV;
using Emgu.CV.Structure;
using ImageProcessingLibrary;

namespace Exercise2.Processing
{
    /// <summary>
    /// Interaction logic for HistogrammEqualization.xaml
    /// </summary>
    public partial class HistogrammEqualization : Window
    {
        private Image<Gray, byte> image;

        public HistogrammEqualization(Image<Gray, byte> image)
        {
            this.InitializeComponent();
            this.image = image;
            this.ApplyHistogramEqualization();
        }

        private void ApplyHistogramEqualization() => this.OwnEqualization();//this.CvInvokeEqualization();

        private void OwnEqualization()
        {
            var histogram = ImageHelper.CreateHistogram(this.image);

            for (int i = 0; i < histogram.Length; i++)
            {
                histogram[i] /= this.image.Rows * this.image.Cols;
            }

            for (int i = 1; i < histogram.Length; i++)
            {
                histogram[i] = histogram[i - 1] + histogram[i];
            }

            for (int i = 0; i < histogram.Length; i++)
            {
                histogram[i] *= histogram.Length;
            }

            for (int i = 0; i < histogram.Length; i++)
            {
                histogram[i] = Math.Ceiling(histogram[i]);
            }

            for (int i = 0; i < histogram.Length; i++)
            {
                histogram[i] -= 1;
            }

            for (int i = 0; i < this.image.Rows; i++)
            {
                for (int j = 0; j < this.image.Cols; j++)
                {
                    var temp = this.image[i, j];
                    temp.Intensity = histogram[(int)temp.Intensity];
                    this.image[i, j] = temp;
                }
            }

            this.SetImage(this.image);
        }

        private void CvInvokeEqualization()
        {
            CvInvoke.EqualizeHist(this.image, this.image);
            this.SetImage(this.image);
        }

        public string GetInformation(Image<Gray, byte> image)
        {
            image.AvgSdv(out var average, out var standardDeviation);
            var builder = new StringBuilder();
            builder.Append("Median Grayscale: ").AppendLine(average.ToString());
            builder.Append("Standard Deviation: ").AppendLine(standardDeviation.V0.ToString());
            return builder.ToString();
        }

        public void SetImage(Image<Gray, byte> image)
        {
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            this.ImageInformation.Text = this.GetInformation(image);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((sender as CheckBox).IsChecked == true)
            {
                this.CvInvokeEqualization();
            }
            else
            {
                this.OwnEqualization();
            }
        }
    }
}