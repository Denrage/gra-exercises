﻿using System.Text;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise2
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : Window
    {
        private Image<Gray, byte> image;

        public ImageView(Image<Gray, byte> image)
        {
            this.image = image;
            this.InitializeComponent();
            this.SetImage(image);
        }

        public void SetImage(Image<Gray, byte> image)
        {
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            this.ImageInformation.Text = this.GetInformation(image);
        }

        public string GetInformation(Image<Gray, byte> image)
        {
            image.AvgSdv(out var average, out var standardDeviation);
            var builder = new StringBuilder();
            builder.Append("Median Grayscale: ").AppendLine(average.ToString());
            builder.Append("Standard Deviation: ").AppendLine(standardDeviation.V0.ToString());
            return builder.ToString();
        }
    }
}