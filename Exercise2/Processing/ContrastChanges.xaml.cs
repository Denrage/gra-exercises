﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise2
{
    /// <summary>
    /// Interaction logic for ContrastChanges.xaml
    /// </summary>
    public partial class ContrastChanges : Window
    {
        private Image<Gray, byte> image;

        public ContrastChanges(Image<Gray, byte> image)
        {
            this.image = image;
            this.InitializeComponent();
            //this.ApplyLinearScaling(this.image);
            this.CvInvokeLookUp(this.image);
        }

        private void ApplyLinearScaling(Image<Gray, byte> image)
        {
            var flattenImage = ImageProcessingLibrary.ImageHelper.FlattenImage<Gray>(image);
            var minimum = flattenImage.Min(x => x.Intensity);
            var maximum = flattenImage.Max(x => x.Intensity);
            var quantilMinimum = flattenImage.Where(x => x.Intensity > minimum * 1.1).Min(x => x.Intensity);
            var quantilMaximum = flattenImage.Where(x => x.Intensity < maximum * 0.9).Max(x => x.Intensity);
            var lookUp = new List<double>();
            for (int i = 0; i < 256; i++)
            {
                lookUp.Add((i - quantilMinimum) * ((255 - 0) / (quantilMaximum - quantilMinimum)));
            }
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    var temp = image[i, j];
                    temp.Intensity = lookUp[(int)temp.Intensity];
                    image[i, j] = temp;
                }
            }

            this.SetImage(image);
        }

        private void CvInvokeLookUp(Image<Gray, byte> image)
        {
            var flattenImage = ImageProcessingLibrary.ImageHelper.FlattenImage<Gray>(image);
            var histogram = ImageProcessingLibrary.ImageHelper.CreateHistogram(image);
            var lowerQuantile = -1;
            var higherQuantile = -1;
            for (int i = 0; i < histogram.Length; i++)
            {
                var result = MathNet.Numerics.Statistics.Statistics.EmpiricalCDF(histogram, i);
                if (result > 0.1 && lowerQuantile == -1)
                {
                    lowerQuantile = i;
                }

                if (result < 0.9 && higherQuantile == -1)
                {
                    higherQuantile = i;
                }
            }
            var minimum = lowerQuantile;
            var maximum = higherQuantile;

            var temp = new double[256];
            var lookUp = new Mat(1, 256, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            for (int i = 0; i < 256; i++)
            {
                temp[i] = (i - minimum) * ((255 - 0) / (maximum - minimum));
            }
            lookUp.SetTo(temp);
            CvInvoke.LUT(image, lookUp, image);
            this.SetImage(image);
        }

        public string GetInformation(Image<Gray, byte> image)
        {
            image.AvgSdv(out var average, out var standardDeviation);
            var builder = new StringBuilder();
            builder.Append("Median Grayscale: ").AppendLine(average.ToString());
            builder.Append("Standard Deviation: ").AppendLine(standardDeviation.V0.ToString());
            return builder.ToString();
        }

        public void SetImage(Image<Gray, byte> image)
        {
            this.Image.Width = image.Cols;
            this.Image.Height = image.Rows;
            this.Image.Source = ImageProcessingLibrary.ImageHelper.ToBitmapSource(image);
            this.ImageInformation.Text = this.GetInformation(image);
        }
    }
}