﻿using System.IO;
using System.Reflection;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Exercise2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string image =
            Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "images",
                "London.bmp");

        public MainWindow()
        {
            this.InitializeComponent();
        }

        public void Original_Click(object sender, RoutedEventArgs e) => new ImageView(this.GetImage()).Show();

        public void LinearScaling_Click(object sender, RoutedEventArgs e) => new LinearScaling(this.GetImage()).Show();

        public void GammaTransformation_Click(object sender, RoutedEventArgs e) => new GammaTransformation(this.GetImage()).Show();

        public void ContrastChanges_Click(object sender, RoutedEventArgs e) => new ContrastChanges(this.GetImage()).Show();

        private Image<Gray, byte> GetImage() =>
            CvInvoke.Imread(this.image, Emgu.CV.CvEnum.ImreadModes.Grayscale).ToImage<Gray, byte>();

        private void HistogramEqualization_Click(object sender, RoutedEventArgs e) => new Processing.HistogrammEqualization(this.GetImage()).Show();
    }
}