﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ImageProcessingLibrary
{
    public static class ImageHelper
    {
        /// <summary>
        /// Delete a GDI object
        /// </summary>
        /// <param name="o">The poniter to the GDI object to be deleted</param>
        /// <returns></returns>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }

        public static IEnumerable<T> FlattenImage<T>(Image<T, byte> image)
            where T : struct, IColor
        {
            var result = new T[image.Rows * image.Cols];
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    result[i + j] = image[i, j];
                }
            }

            return result;
        }

        public static double[] CreateHistogram(Image<Gray, byte> image)
        {
            var result = new double[256];
            for (int i = 0; i < image.Rows; i++)
            {
                for (int j = 0; j < image.Cols; j++)
                {
                    result[(int)image[i, j].Intensity]++;
                }
            }
            return result;
        }

        public static Image<TColor, TDepth> AdjustContrast<TColor, TDepth>(IImage image)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var min = double.MaxValue;
            var max = double.MinValue;
            var nullPoint = new System.Drawing.Point();
            var result = new Image<TColor, TDepth>(image.Size);
            CvInvoke.MinMaxLoc(image, ref min, ref max, ref nullPoint, ref nullPoint);
            CvInvoke.ConvertScaleAbs(image, result, 255d / (max - min), -min * 255d / (max - min));
            return result;
        }
    }
}