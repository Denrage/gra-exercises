﻿using System.Runtime.InteropServices;
using Emgu.CV;

namespace ImageProcessingLibrary
{
    public static class Extensions
    {
        public static void SetDoubleValue(this Mat mat, int row, int col, double value)
        {
            var target = new[] { value };
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }
    }
}